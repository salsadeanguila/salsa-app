import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CategoriesPage } from '../pages/categories/categories';
import { ItemsPage } from '../pages/items/items';
import { ItemDetailPage } from '../pages/item-detail/item-detail';

import { AppConfig } from '../providers/app-config';
import { CategoriesService } from '../providers/categories';
import { ItemsService } from '../providers/items';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CategoriesPage,
    ItemsPage,
    ItemDetailPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: 'Regresar',
      swipeBackEnabled: true
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CategoriesPage,
    ItemsPage,
    ItemDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AppConfig,
    CategoriesService,
    ItemsService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
