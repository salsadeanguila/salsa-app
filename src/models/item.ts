export class Item {
  constructor(
    public name: string,
    public description: string,
    public category_id: number,
    public active: boolean,
    public price: number,
    public image_url: string,
    public item_type: string
  ){}
}
