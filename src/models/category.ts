export class Category {
  constructor(
    public name: string,
    public description: string,
    public image_url: string,
    public menu_order: number
  ){}
}
