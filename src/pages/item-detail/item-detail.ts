import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Item } from '../../models/item';

import { ItemsService } from '../../providers/items';


@IonicPage()
@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html',
})
export class ItemDetailPage {
  itemId: any;
  categoryId: any;
  currentItem: Item;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public itemsService: ItemsService) {
      this.itemId = this.navParams.get('itemId');
      this.categoryId = this.navParams.get('categoryId');
  }

  ionViewDidLoad() {
    this.getItem(this.itemId, this.categoryId);
  }

  getItem(itemId, categoryId){
    this.itemsService.getItem(itemId, categoryId)
      .subscribe(result => {
        if (result) {
          this.currentItem = result;
          console.log(this.currentItem);
        }
      },
      err => {
        console.log("Error");
      });
  }

}
