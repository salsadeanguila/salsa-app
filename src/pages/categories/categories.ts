import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Category } from '../../models/category';

import { CategoriesService } from '../../providers/categories';

import { ItemsPage } from '../items/items';


@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html'
})
export class CategoriesPage {
  categories: Category[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public categoriesService: CategoriesService) {
  }

  ionViewDidLoad() {
    this.getCategories();
  }

  getCategories(){
    this.categoriesService.getCategories()
      .subscribe(results => {
        if (results) {
          this.categories = results;
          // SORT CATEGORIES TO USE MENU_ORDER OR CHANGE ORDERING ON API
          this.categories.sort(function(a,b) {
            return a.menu_order - b.menu_order;
          });
        }
      },
      err => {
        console.log("Error");
      });
  }

  categoryTapped(event, categoryId) {
    this.navCtrl.push(ItemsPage, {
      categoryId: categoryId
    });
  }

}
