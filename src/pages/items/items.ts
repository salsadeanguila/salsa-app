import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Item } from '../../models/item';

import { ItemsService } from '../../providers/items';

import { ItemDetailPage } from '../item-detail/item-detail';


@IonicPage()
@Component({
  selector: 'page-items',
  templateUrl: 'items.html',
})
export class ItemsPage {
  categoryId: any;
  items: Item[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public itemsService: ItemsService) {
    this.categoryId = this.navParams.get('categoryId');
  }

  ionViewDidLoad() {
    this.getCategoryItems();
  }

  getCategoryItems(){
    this.itemsService.getCategoryItems(this.categoryId)
      .subscribe(results => {
        for(let result of results){
          // Solo muestra los ítems activos
          if (result.active == true) {
            this.items.push(result);
          }
        }
      },
      err => {
        console.log("Error");
      });
  }

  itemTapped(event, itemId, categoryId) {
    this.navCtrl.push(ItemDetailPage, {
      itemId: itemId,
      categoryId: categoryId
    });
  }

}
