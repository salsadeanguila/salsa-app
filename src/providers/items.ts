import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { AppConfig } from './app-config';

import { Item } from '../models/item';

@Injectable()
export class ItemsService {
  apiURL: string;

  constructor(
    public http: Http,
    public appConfig: AppConfig) {
      this.apiURL = this.appConfig.API_URL;
  }

  getCategoryItems(categoryId: string): Observable<Item[]> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let categoryItemsURL = `${this.apiURL}/categories/${categoryId}/items`;

    return this.http.get(categoryItemsURL, options)
      .map(this.extractData);
  }

  getItem(itemId: string, categoryId: string): Observable<Item> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let itemURL = `${this.apiURL}/categories/${categoryId}/items/${itemId}`;

    return this.http.get(itemURL, options)
      .map(this.extractData);
  }

  private extractData(response: Response) {
    let body = response.json();
    return body || {};
  }

}
