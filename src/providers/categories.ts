import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { AppConfig } from './app-config';

import { Category } from '../models/category';

@Injectable()
export class CategoriesService {
  apiURL: string;
  categoriesURL: string;

  constructor(
    public http: Http,
    public appConfig: AppConfig) {
      this.apiURL = this.appConfig.API_URL;
      this.categoriesURL = `${this.apiURL}/categories`;
  }

  getCategories(): Observable<Category[]> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers })
    return this.http.get(this.categoriesURL, options)
      .map(this.extractData);
  }

  private extractData(response: Response) {
    let body = response.json();
    return body || {};
  }

}
