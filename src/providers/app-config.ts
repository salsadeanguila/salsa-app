import { Injectable } from '@angular/core';

@Injectable()
export class AppConfig {

  public API_URL: string = "https://flordlerma-api.herokuapp.com";

  constructor() {

  }

}
